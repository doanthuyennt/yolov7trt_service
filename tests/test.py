import requests

url = "http://192.168.20.150:6000/detect/predict_images/"

files = [("binary_files", open("./tests/horses.jpg", "rb")),
         ("binary_files", open("./tests/bus.jpg", "rb"))]


response = requests.request("GET", url, files=files)

print(response.text)
