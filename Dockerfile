FROM waikatodatamining/pytorch-yolov7:2022-10-08_cuda11.1

ENV PYTHONIOENCODING=UTF-8 \
    LANG=C.UTF-8 \
    TZ="Asia/Ho_Chi_Minh"

RUN apt install git -y

RUN pip install --upgrade pip

RUN pip install --upgrade setuptools pip --user
RUN pip install nvidia-pyindex
RUN pip install --upgrade nvidia-tensorrt
RUN pip install pycuda

RUN pip install fastapi "uvicorn[standard]" multipledispatch
RUN pip install python-multipart
WORKDIR /app
EXPOSE 6000
CMD ["uvicorn","main:app","--port","6000","--host","0.0.0.0","--workers","1","--reload"]