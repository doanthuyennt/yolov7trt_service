from cgitb import handler
from logging import getLogger, INFO, DEBUG
from logging import Formatter
from logging.handlers import RotatingFileHandler

from datetime import date
from pathlib import Path

from src.utils.config import get_app_config

config = get_app_config()


def get_filename(postfix):
    today = date.today()
    return f"{config['log']['dir']}/{today.strftime('%Y-%m-%d')}.{postfix}.log"


def get_logger(name: str = None):
    logger = getLogger(name)
    log_level = DEBUG
    logger.setLevel(log_level)
    logger.info("START")
    fmt_str = "%(asctime)s %(levelname)s" " %(filename)s %(funcName)s(%(lineno)d) %(message)s"
    formater = Formatter(fmt_str)

    path = get_filename(name)

    Path(path).parent.mkdir(parents=True, exist_ok=True)

    handler = RotatingFileHandler(
        filename=path,
        maxBytes=512 * 1024,
        backupCount=200,
        delay=False,
    )
    handler.setFormatter(formater)
    handler.setLevel(log_level)
    logger.addHandler(handler)

    return logger