import yaml
from typing import Union
from pathlib import Path


def _parse_config(config_path: Union[Path, str]):
    with open(config_path, "r") as f:
        configs = yaml.safe_load(f)
    return configs


def get_app_config():
    return _parse_config("configs/app.yml")
