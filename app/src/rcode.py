from multipledispatch import dispatch

class ErrorCode(object):
    def __init__(self, int_code, str_code, message):

        self.int_code = int_code
        self.str_code = str_code
        self.message = message

    def get_print(self):
        return {
            "code": self.int_code,
            "str_code": self.str_code,
            "message": self.message,
        }


class ErrorList(object):
    def __init__(self):
        self.int_list = {}
        self.str_list = {}

    def add_error(self, new_error):
        self.int_list[new_error.int_code] = new_error
        self.str_list[new_error.str_code] = new_error

    @dispatch(int)
    def __call__(self, code):
        return self.int_list[code].get_print()

    @dispatch(str)
    def __call__(self, code):
        return self.str_list[code].get_print()


rcode = ErrorList()

c409 = ErrorCode(int_code=409, str_code="ErrorDuringUploadFile", message="Error During Upload File")
rcode.add_error(c409)

c415 = ErrorCode(int_code=415, str_code="UnsupportedMediaType", message="Unsupported Media Type")
rcode.add_error(c415)


c609 = ErrorCode(int_code=609, str_code="WrongInput", message="Wrong Input")
rcode.add_error(c609)



c1000 = ErrorCode(int_code=1000, str_code="Done", message="Done")
rcode.add_error(c1000)

c1001 = ErrorCode(int_code=1001, str_code="ServerGotUndeterminedError", message="Server Got Undetermined Error")
rcode.add_error(c1001)

c1201 = ErrorCode(int_code=1201, str_code="FileNotFound", message="File Not Found")
rcode.add_error(c1201)



if __name__ == "__main__":
    print(rcode(609))
    print(rcode("WrongInput"))
