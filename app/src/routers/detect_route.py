import torch
import tensorrt as trt
import numpy as np
import cv2
import os
from fastapi import APIRouter
from fastapi import File, UploadFile


from typing import List, Dict
from collections import OrderedDict, namedtuple

from src.rcode import rcode
from src.logger import get_logger
from src.utils.config import get_app_config

from src.routers.detect_utils import postprocess, letterbox, convert_class_id_to_names


logger = get_logger(__name__)

router = APIRouter()


config = get_app_config()

device = torch.device('cuda')

WEIGHTS = config.get("weights")
CATEGORIES = config.get("categories")
os.environ["CUDA_VISIBLE_DEVICES"] = config.get("device_id")


logger_ = trt.Logger(trt.Logger.INFO)
trt.init_libnvinfer_plugins(logger_, namespace="")
with open(WEIGHTS, 'rb') as f, trt.Runtime(logger_) as runtime:
    model = runtime.deserialize_cuda_engine(f.read())
    context = model.create_execution_context()
logger.info("Finish loading model.")

Binding = namedtuple('Binding', ('name', 'dtype', 'shape', 'data', 'ptr'))
bindings = OrderedDict()
for index in range(model.num_bindings):
    name = model.get_binding_name(index)
    dtype = trt.nptype(model.get_binding_dtype(index))
    shape = tuple(model.get_binding_shape(index))
    data = torch.from_numpy(np.empty(shape, dtype=np.dtype(dtype))).to(device)
    bindings[name] = Binding(name, dtype, shape, data, int(data.data_ptr()))
binding_addrs = OrderedDict((n, d.ptr) for n, d in bindings.items())


def yolov7_tensort_detect(img, threshold: float) -> Dict:

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    image = img.copy()
    image, ratio, dwdh = letterbox(image, auto=False)
    image = image.transpose((2, 0, 1))
    image = np.expand_dims(image, 0)
    image = np.ascontiguousarray(image)

    im = image.astype(np.float32)

    # # im.shape
    im = torch.from_numpy(im).to(device)
    im /= 255

    binding_addrs['images'] = int(im.data_ptr())
    context.execute_v2(list(binding_addrs.values()))

    nums = bindings['num_dets'].data
    boxes = bindings['det_boxes'].data
    scores = bindings['det_scores'].data
    classes = bindings['det_classes'].data

    boxes = boxes[0, :nums[0][0]]
    scores = scores[0, :nums[0][0]]
    classes = classes[0, :nums[0][0]]

    return_data = list()
    for box, score, cl in zip(boxes, scores, classes):
        if score < threshold:
            continue
        box = postprocess(box, ratio, dwdh).round().int()
        box = box.tolist()
        object_cls = convert_class_id_to_names(int(cl),CATEGORIES)
        return_data.append(
            dict(box=box, score=round(float(score),2), object_class=object_cls)
        )
    return return_data


@router.get("/predict_images/")
async def predict_images(
    binary_files: List[UploadFile] = File(...),
    threshold: float = 0.,
):
    return_data = {}
    # Read upload files here:
    try:
        try:
            bytes_files = list()
            for binary_file in binary_files:
                bytes_file = await binary_file.read()
                bytes_files.append(bytes_file)
            process_images = list()
            for byte_file in bytes_files:
                nparr = np.fromstring(byte_file, np.uint8)
                process_image = cv2.imdecode(nparr, flags=1)
                process_images.append(process_image)

        except Exception as e:
            logger.info(e, exc_info=True)
            return_data = dict(**rcode("ServerGotUndeterminedError"))
            return return_data

        # Use Yolov7 tensorrt here:
        try:
            results = list()
            for img in process_images:
                results.append(yolov7_tensort_detect(img,threshold))

            return_data = dict(**rcode(1000), results=results)
        except Exception as e:
            logger.info(e, exc_info=True)
            return_data = dict(**rcode("ServerGotUndeterminedError"))


    except Exception as e:
        logger.info(e, exc_info=True)
        return_data = dict(**rcode("ServerGotUndeterminedError"))

    return return_data
