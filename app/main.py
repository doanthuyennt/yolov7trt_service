from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware



from src.logger import get_logger

import src.routers.detect_route as detect_route


from logging import Logger


logger = get_logger(__name__)


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins="*",
    allow_credentials=True,
    allow_methods=[
        "GET", "POST"
    ],
    allow_headers = "*",
)

app.include_router(detect_route.router, prefix="/detect")

@app.get("/")
def index():
    return {}
