# Run:
docker-compose up --build

# Api:
- Object detection
    + detect/predict_images/
    
    + return: 
    ```
    "code": int,
    "str_code": string,
    "message": string,
    "results":
    list[
        list['box': list[int], 
            'score': float,
            'object_class': string]
        ]
        ...
    ```
